export interface DetailCompoentPresenter {
    titleDetail: string;
    author: any;
    detailCover: string
    category: string;
    detailMarkdown: string;
}