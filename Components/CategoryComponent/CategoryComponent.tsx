import Link from "next/link";
import "./categoryComponent.scss";
const CategoryComponent = ({
    categoryComponentPresenter
}: any) => {
    return (
        <div className="wrapper-content-category mb-5">
            <div className="items-in-category">
                <Link href="/detail">
                    <a>
                        <div className="wrapper-image">
                            <img src="https://images.unsplash.com/photo-1515879218367-8466d910aaa4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80" className="w-100" alt="" />
                        </div>
                    </a>
                </Link>
                <div className="wrapper-button-plus mb-3">
                    <button className="btn-plus">
                        +
                </button>
                </div>
                <p className="category-name">
                    Development
                </p>
                <p>
                    5 Amazing Places To Visit In Summer
                </p>
                <p className="text-muted">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto atque quas eligendi saepe veritatis. Praesentium tempora esse iusto iure quas voluptates quisquam, officia veritatis, fugiat assumenda quo suscipit deserunt. Doloribus.
                </p>
                <div className="d-flex">
                    <div className="text-secondary">
                        John Doh    November , 8 , 2019
                    </div>
                </div>
            </div>            
            
            <div className="items-in-category">
                <Link href="/detail">
                    <a>
                        <div className="wrapper-image">
                            <img src="https://images.unsplash.com/photo-1572044162444-ad60f128bdea?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80" className="w-100" alt="" />
                        </div>
                    </a>
                </Link>
                <div className="wrapper-button-plus mb-3">
                    <button className="btn-plus">
                        +
                </button>
                </div>
                <p className="category-name">
                    Designer
                </p>
                <p>
                    5 Amazing Places To Visit In Summer
                </p>
                <p className="text-muted">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto atque quas eligendi saepe veritatis. Praesentium tempora esse iusto iure quas voluptates quisquam, officia veritatis, fugiat assumenda quo suscipit deserunt. Doloribus.
                </p>
                <div className="d-flex">
                    <div className="text-secondary">
                        John Doh    November , 8 , 2019
                    </div>
                </div>
            </div>            

            <div className="items-in-category">
                <Link href="/detail">
                    <a>
                        <div className="wrapper-image">
                            <img src="https://images.unsplash.com/photo-1524749292158-7540c2494485?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80" className="w-100" alt="" />
                        </div>
                    </a>
                </Link>
                <div className="wrapper-button-plus mb-3">
                    <button className="btn-plus">
                        +
                </button>
                </div>
                <p className="category-name">
                    Business
                </p>
                <p>
                    5 Amazing Places To Visit In Summer
                </p>
                <p className="text-muted">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto atque quas eligendi saepe veritatis. Praesentium tempora esse iusto iure quas voluptates quisquam, officia veritatis, fugiat assumenda quo suscipit deserunt. Doloribus.
                </p>
                <div className="d-flex">
                    <div className="text-secondary">
                        John Doh    November , 8 , 2019
                    </div>
                </div>
            </div>            
            
            <div className="items-in-category">
                <Link href="/detail">
                    <a>
                        <div className="wrapper-image">
                            <img src="https://images.unsplash.com/photo-1502068898470-ad70c83938be?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1052&q=80" className="w-100" alt="" />
                        </div>
                    </a>
                </Link>
                <div className="wrapper-button-plus mb-3">
                    <button className="btn-plus">
                        +
                </button>
                </div>
                <p className="category-name">
                    Machanical
                </p>
                <p>
                    5 Amazing Places To Visit In Summer
                </p>
                <p className="text-muted">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto atque quas eligendi saepe veritatis. Praesentium tempora esse iusto iure quas voluptates quisquam, officia veritatis, fugiat assumenda quo suscipit deserunt. Doloribus.
                </p>
                <div className="d-flex">
                    <div className="text-secondary">
                        John Doh    November , 8 , 2019
                    </div>
                </div>
            </div>            
            
            <div className="items-in-category">
                <Link href="/detail">
                    <a>
                        <div className="wrapper-image">
                            <img src="https://images.unsplash.com/photo-1506097425191-7ad538b29cef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80" className="w-100" alt="" />
                        </div>
                    </a>
                </Link>
                <div className="wrapper-button-plus mb-3">
                    <button className="btn-plus">
                        +
                </button>
                </div>
                <p className="category-name">
                    Machanical
                </p>
                <p>
                    5 Amazing Places To Visit In Summer
                </p>
                <p className="text-muted">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto atque quas eligendi saepe veritatis. Praesentium tempora esse iusto iure quas voluptates quisquam, officia veritatis, fugiat assumenda quo suscipit deserunt. Doloribus.
                </p>
                <div className="d-flex">
                    <div className="text-secondary">
                        John Doh    November , 8 , 2019
                    </div>
                </div>
            </div>            


            <div className="items-in-category">
                <Link href="/detail">
                    <a>
                        <div className="wrapper-image">
                            <img src="https://images.unsplash.com/photo-1516534775068-ba3e7458af70?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" className="w-100" alt="" />
                        </div>
                    </a>
                </Link>
                <div className="wrapper-button-plus mb-3">
                    <button className="btn-plus">
                        +
                </button>
                </div>
                <p className="category-name">
                    Creative
                </p>
                <p>
                    5 Amazing Places To Visit In Summer
                </p>
                <p className="text-muted">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto atque quas eligendi saepe veritatis. Praesentium tempora esse iusto iure quas voluptates quisquam, officia veritatis, fugiat assumenda quo suscipit deserunt. Doloribus.
                </p>
                <div className="d-flex">
                    <div className="text-secondary">
                        John Doh    November , 8 , 2019
                    </div>
                </div>
            </div>            
        </div>
    )
}

export default CategoryComponent