export enum KeyManager {
    EmailSignin = 'emailSignin',
    PasswordSignin = 'passwordSignin',
    FirstNameSignup = 'firstNameSignup',
    LastNameSignup = 'lastNameSignup',
    EmailSignup = 'emailSignup',
    PhoneSignup = 'phoneSignup',
    PasswordSignup = 'passwordSignup',
    ConfirmPasswordSignup = 'confirmPasswordSignup',
}